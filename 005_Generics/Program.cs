﻿using System;

// Ковариантность обобщений.
// Covariant Generics

//covariant, contravariant, invariant

namespace _005_Generics
{
    public abstract class Shape { }
    public class Rectangle : Shape { }
    public class Triangle : Shape { }

    public interface IContainer<out T>
    {
        T Figure { get; }
    }

    public class Container<T> : IContainer<T>
    {
        public T Figure { get; set; }

        public Container(T figure)
        {
            this.Figure = figure;
        }
    }

    class Program
    {
        static void Main()
        {
            Shape shape = new Rectangle();
            Triangle tri = (Triangle)shape;

            Rectangle rectangle = new Rectangle();
            Triangle triangle = new Triangle();

            IContainer<Shape> containerR = new Container<Rectangle>(rectangle);
            IContainer<Shape> containerT = new Container<Triangle>(triangle);

            //containerR.Figure = triangle;

            Console.WriteLine(containerR.Figure.ToString());

            Method(containerR);
            Method(containerT);

            // Delay.
            Console.ReadKey();
        }

        static void Method(IContainer<Shape> container)
        {

        }
    }
}
