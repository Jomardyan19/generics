﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> stList = CreateStudents(10);
            var stListCopy = stList.Copy();

            List<Teacher> tList = CreateTeachers(10);
            var tListCopy = tList.Copy();

            ICopyable<Student> copyable = new Student();
        }

        static List<Student> CreateStudents(int count)
        {
            var list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                var st = new Student { Name = $"A{i + 1}", Surname = $"A{i + 1}yan" };
                list.Add(st);
            }
            return list;
        }

        static List<Teacher> CreateTeachers(int count)
        {
            var list = new List<Teacher>(count);
            for (int i = 0; i < count; i++)
            {
                var st = new Teacher { Name = $"T{i + 1}", Surname = $"T{i + 1}yan" };
                list.Add(st);
            }
            return list;
        }
    }
}
