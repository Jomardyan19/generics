﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class MyDoc
    {
        public string Title { get; set; }
        public string Body { get; set; }

        public MyDoc Copy()
        {
            return MemberwiseClone() as MyDoc;
        }
    }
}