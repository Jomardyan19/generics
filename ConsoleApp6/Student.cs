﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Student : ICopyable<Student>
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public string Fullname => $"{Surname} {Name}";

        public Student Copy()
        {
            return base.MemberwiseClone() as Student;
        }

        public override string ToString()
        {
            return Fullname;
        }
    }
}
