﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    static class ListExtensions
    {
        public static T Test<T>(this List<T> source)
            where T : class, new()
        {
            T t = new T();
            return default(T);
        }

        public static List<T> Copy<T>(this List<T> source)
            where T : class, ICopyable<T>
        {
            var list = new List<T>(source.Count);
            foreach (T item in source)
            {
                list.Add(item.Copy());
            }
            return list;
        }

        //public static List<Student> Copy(this List<Student> source)
        //{
        //    var list = new List<Student>(source.Count);
        //    foreach (Student item in source)
        //    {
        //        list.Add(item.Copy());
        //    }
        //    return list;
        //}
        
        //public static List<Teacher> Copy(this List<Teacher> source)
        //{
        //    var list = new List<Teacher>(source.Count);
        //    foreach (Teacher item in source)
        //    {
        //        list.Add(item.Copy());
        //    }
        //    return list;
        //}

        //public static List<MyDoc> Copy(this List<MyDoc> source)
        //{
        //    var list = new List<MyDoc>(source.Count);
        //    foreach (MyDoc item in source)
        //    {
        //        list.Add(item.Copy());
        //    }
        //    return list;
        //}
    }
}