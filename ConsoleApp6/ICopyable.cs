﻿namespace ConsoleApp6
{
    public interface ICopyable<out T>
        where T : class
    {
        T Copy();
    }
}