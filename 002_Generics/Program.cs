﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _002_Generics
{
    class MyGenerics<TValue1, TValue2>
    {
        private TValue1 variable1;
        private TValue2 variable2;

        public MyGenerics(TValue1 argument1, TValue2 argument2)
        {
            this.variable1 = argument1;
            this.variable2 = argument2;
        }

        public TValue1 Variable1
        {
            get { return this.variable1; }
            set { this.variable1 = value; }
        }

        public TValue2 Variable2
        {
            get { return this.variable2; }
            set { this.variable2 = value; }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var instance1 = new MyGenerics<int, int>(1, 2);
            Console.WriteLine(instance1.Variable1 + instance1.Variable2);

            var instance2 = new MyGenerics<string, int>("Number ", 1);
            Console.WriteLine(instance2.Variable1 + instance2.Variable2);

            var instance3 = new MyGenerics<string, string>("Hello ", "World");
            Console.WriteLine(instance3.Variable1 + instance3.Variable2);

            Console.ReadKey();
        }
    }
}
