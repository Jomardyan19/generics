﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _001_Generics
{
    class MyClass<T>
    {
        public T field;

        public void Method()
        {
            Console.WriteLine(field.GetType());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyClass<int> instance1 = new MyClass<int>();
            instance1.Method();
            int f1 = instance1.field;

            MyClass<long> instance2 = new MyClass<long>();
            instance2.Method();
            long f2 = instance2.field;

            MyClass<string> instance3 = new MyClass<string>();
            instance3.field = "ABC";
            instance3.Method();

            Console.ReadKey();
        }
    }
}