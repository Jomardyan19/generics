﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Teacher : ICopyable<Teacher>
    {
        public Teacher(string name)
        {

        }

        public string Name { get; set; }
        public string Surname { get; set; }

        public string Fullname => $"{Surname} {Name}";

        public Teacher Copy()
        {
            return base.MemberwiseClone() as Teacher;
        }

        public override string ToString()
        {
            return Fullname;
        }
    }
}
