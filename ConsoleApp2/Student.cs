﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student : ICopyable<Student>
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public string Fullname => $"{Surname} {Name}";

        public Student Copy()
        {
            return base.MemberwiseClone() as Student;
            //return new Student { Name = this.Name, Surname = this.Surname };
        }

        public override string ToString()
        {
            return Fullname;
        }
    }
}
