﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    static class CollectionExtensions
    {
        public static List<T> Copy<T>(this List<T> source)
            where T : ICopyable<T>
        {
            var items = new List<T>(source.Count);
            foreach (T item in source)
            {
                items.Add(item.Copy());
            }
            return items;
        }

        //public static List<Student> Copy(this List<Student> source)
        //{
        //    var items = new List<Student>(source.Count);
        //    foreach (Student item in source)
        //    {
        //        items.Add(item.Copy());
        //    }
        //    return items;
        //}

        //public static List<Teacher> Copy(this List<Teacher> source)
        //{
        //    var items = new List<Teacher>(source.Count);
        //    foreach (Teacher item in source)
        //    {
        //        items.Add(item.Copy());
        //    }
        //    return items;
        //}
    }
}