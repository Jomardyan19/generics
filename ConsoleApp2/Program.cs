﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //var st = new Student { Name = "A1", Surname = "A1yan" };

            ////var st1 = new Student { Name = st.Name, Surname = st.Surname };
            //var st1 = st.Copy();

            List<Student> sts1 = CreateStudents(10);
            List<Student> sts2 = sts1.Copy();

            List<Teacher> ts1 = CreateTeachers(10);
            List<Teacher> ts2 = ts1.Copy();
        }

        static List<Student> CreateStudents(int count)
        {
            var list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                var st = new Student { Name = $"A{i + 1}", Surname = $"A{i + 1}yan" };
                list.Add(st);
            }
            return list;
        }

        static List<Teacher> CreateTeachers(int count)
        {
            var list = new List<Teacher>(count);
            for (int i = 0; i < count; i++)
            {
                var st = new Teacher { Name = $"T{i + 1}", Surname = $"T{i + 1}yan" };
                list.Add(st);
            }
            return list;
        }
    }
}
