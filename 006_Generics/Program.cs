﻿using System;

// Ковариантность обобщений.
// Covariant Generics

//covariant, contravariant, invariant

namespace _006_Generics
{
    public abstract class Shape { }
    public class Rectangle : Shape { }

    public interface IContainer<out T>
    {
        T Figure { get; }
    }

    public class Container<T> : IContainer<T>
    {
        private T figure;

        public Container(T figure)
        {
            this.figure = figure;
        }

        public T Figure
        {
            get { return figure; }
        }
    }

    class Program
    {
        static void Main()
        {
            Rectangle rectangle = new Rectangle();

            IContainer<Shape> container = new Container<Rectangle>(rectangle);

            Console.WriteLine(container.Figure.ToString());

            // Delay.
            Console.ReadKey();
        }
    }
}
