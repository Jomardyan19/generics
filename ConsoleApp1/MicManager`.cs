﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MicManager<T>
    {
        public void Shuffle(T[] source)
        {
            Random rnd = new Random();
            for (int i = source.Length - 1; i > 0; i--)
            {
                int j = rnd.Next(i + 1);

                var temp = source[i];
                source[i] = source[j];
                source[j] = temp;
            }
        }
    }
}
