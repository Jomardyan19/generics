﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = { 10, 20, 30, 40, 50 };
            long[] arr2 = { 10, 20, 30, 40, 50 };

            var man1 = new MicManager<int>();
            man1.Shuffle(arr1);

            var man2 = new MicManager<long>();

            man2.Shuffle(arr2);

            var man = new MicManager();
            man.Shuffle(arr1);
            man.Shuffle(arr2);

            Console.WriteLine(man1.GetType());
            Console.WriteLine(man2.GetType());
            Console.WriteLine(man.GetType());

            Console.ReadLine();
            Console.WriteLine();
        }
        
    }
}
