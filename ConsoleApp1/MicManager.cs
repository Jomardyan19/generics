﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MicManager
    {
        public void Shuffle<T>(T[] source)
        {
            Random rnd = new Random();
            for (int i = source.Length - 1; i > 0; i--)
            {
                int j = rnd.Next(i + 1);
                Swap(source, i, j);
            }
        }

        private void Swap<T>(T[] source, int i, int j)
        {
            var temp = source[i];
            source[i] = source[j];
            source[j] = temp;
        }
    }
}