﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { };
            Result<int, int> res = GetMinAndMax(arr);

            long[] arrL = { };
            Result<long, long> resL = GetMinAndMax(arrL);
            //ResultMinMax<long, long> resL = GetMinAndMax(arrL);
            
        }

        static void GetMinAndMax(int[] arr, out int min, out int max)
        {
            min = 10;
            max = 100;
            //...
        }

        //static ResultMinMax<long, long> GetMinAndMax(long[] arr)
        //{
        //    long min = 10;
        //    long max = 100;
        //    ...
        //    return new ResultMinMax<long, long>(min, max);
        //}

        //static Result<int, int> GetMinAndMax(int[] arr)
        //{
        //    int min = 10;
        //    int max = 100;
        //    //...
        //    return new ResultMinMax<int, int>(min, max);
        //}

        static Result<long, long> GetMinAndMax(long[] arr)
        {
            long min = 10;
            long max = 100;
            //...
            return new Result<long, long>(min, max);
        }

        static Result<int, int> GetMinAndMax(int[] arr)
        {
            int min = 10;
            int max = 100;
            //...
            return new Result<int, int>(min, max);
        }
    }

    class Result<T1, T2>
    {
        public Result(T1 item1, T2 item2)
        {
            this.item1 = item1;
            this.item2 = item2;
        }

        public readonly T1 item1;
        public readonly T2 item2;
    }

    //class ResultMinMax<T1, T2>
    //{
    //    public ResultMinMax(T1 min, T2 max)
    //    {
    //        this.min = min;
    //        this.max = max;
    //    }

    //    public readonly T1 min;
    //    public readonly T2 max;
    //}

    //class ResultSumAverage<T1, T2>
    //{
    //    public ResultSumAverage(T1 sum, T2 average)
    //    {
    //        this.sum = sum;
    //        this.average = average;
    //    }

    //    public readonly T1 sum;
    //    public readonly T2 average;
    //}
}
