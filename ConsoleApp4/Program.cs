﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { };
            Tuple<int, int> res = GetMinAndMax(arr);
            

            long[] arrL = { };
            Tuple<long, long> resL = GetMinAndMax(arrL);
        }

        static Tuple<long, long> GetMinAndMax(long[] arr)
        {
            long min = 10;
            long max = 100;
            //...
            //return new Tuple<long, long>(min, max);
            return Tuple.Create(min, max);
        }

        static Tuple<int, int> GetMinAndMax(int[] arr)
        {
            int min = 10;
            int max = 100;
            //...
            return new Tuple<int, int>(min, max);
        }

        static Tuple<T1, T2> Create<T1, T2>(T1 i1, T2 i2)
        {
            return new Tuple<T1, T2>(i1, i2);
        }
    }
}
